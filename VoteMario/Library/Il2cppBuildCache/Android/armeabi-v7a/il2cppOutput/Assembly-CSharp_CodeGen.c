﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BackGround_parallax::Start()
extern void BackGround_parallax_Start_mD4AC5930AF76396735290F1AD743EA7903A82110 (void);
// 0x00000002 System.Void BackGround_parallax::Update()
extern void BackGround_parallax_Update_m84021604B415F4C5F362A4D9D4ADD9798175C803 (void);
// 0x00000003 System.Void BackGround_parallax::.ctor()
extern void BackGround_parallax__ctor_m8F34171063BC9B4BD7CB79C1094CFE43307E9A6F (void);
// 0x00000004 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x00000005 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x00000006 System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x00000007 System.Void GameManager::Restart()
extern void GameManager_Restart_m1E9741B5443E1FF65B0BE1A20A8F584C90394654 (void);
// 0x00000008 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000009 System.Void GameManager::.cctor()
extern void GameManager__cctor_m0872BB5B2E20B9183F004EB81D5129608114D285 (void);
// 0x0000000A System.Void ObsatcleSpawner::OnEnable()
extern void ObsatcleSpawner_OnEnable_m50FCC755781FEE071FAD9473951A75D227223D5A (void);
// 0x0000000B System.Void ObsatcleSpawner::Start()
extern void ObsatcleSpawner_Start_m27EA92EBEEB031CE9408B7E398A26CA6E40AE6CE (void);
// 0x0000000C System.Collections.IEnumerator ObsatcleSpawner::Spawner()
extern void ObsatcleSpawner_Spawner_mBEFCD361A47CA2E056CB3B5ADF2963B79F82F05C (void);
// 0x0000000D System.Void ObsatcleSpawner::.ctor()
extern void ObsatcleSpawner__ctor_m4928465F57D2F3EB03D81282E86BA7958DB4FE8B (void);
// 0x0000000E System.Void ObsatcleSpawner/<Spawner>d__7::.ctor(System.Int32)
extern void U3CSpawnerU3Ed__7__ctor_mA6797C46ACF74131A3985AFD7321DC257B8FC5F8 (void);
// 0x0000000F System.Void ObsatcleSpawner/<Spawner>d__7::System.IDisposable.Dispose()
extern void U3CSpawnerU3Ed__7_System_IDisposable_Dispose_mD528048A4D6B2750F82476C03E26D5D5500C008F (void);
// 0x00000010 System.Boolean ObsatcleSpawner/<Spawner>d__7::MoveNext()
extern void U3CSpawnerU3Ed__7_MoveNext_mDFC28C95F5DEE4A504E32C44D1E0231799E82C56 (void);
// 0x00000011 System.Object ObsatcleSpawner/<Spawner>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF51FAF03F53A2B24C7FEBB503CACDFDE3C45013C (void);
// 0x00000012 System.Void ObsatcleSpawner/<Spawner>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSpawnerU3Ed__7_System_Collections_IEnumerator_Reset_m43B5CB3EC7C781E60243B53A52E33129008BFC84 (void);
// 0x00000013 System.Object ObsatcleSpawner/<Spawner>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnerU3Ed__7_System_Collections_IEnumerator_get_Current_m31FEB79F32570644AC62FFD36984615EA9394524 (void);
// 0x00000014 System.Void ObstacleProperties::OnEnable()
extern void ObstacleProperties_OnEnable_mF6B61D2B058D55BA245BC0F4282A18E7194DDE07 (void);
// 0x00000015 System.Void ObstacleProperties::Update()
extern void ObstacleProperties_Update_m668DC200D060A2CFBD3E091A14D75323EC45315E (void);
// 0x00000016 System.Void ObstacleProperties::.ctor()
extern void ObstacleProperties__ctor_mBEEDBE4F11963E6A274E7A13AE1B0D403216030A (void);
// 0x00000017 System.Void OnInvisible::LateUpdate()
extern void OnInvisible_LateUpdate_mB0614738B7C25BFADC80EFECCF7DA309D6ABF634 (void);
// 0x00000018 System.Void OnInvisible::OnBecameInvisible()
extern void OnInvisible_OnBecameInvisible_mAD9D7123BD6721FA3917E0933D95FDDFF916F8BF (void);
// 0x00000019 System.Void OnInvisible::.ctor()
extern void OnInvisible__ctor_m1D1432C7922295E59AD498CBAA2DB00EABB8E58F (void);
// 0x0000001A System.Void PoolManager::Awake()
extern void PoolManager_Awake_m786B4A25FE5ECC36089F119C13947812B49CC13C (void);
// 0x0000001B System.Void PoolManager::Start()
extern void PoolManager_Start_m99A69714EE1C8A780EBABB4006D309BF67A56DF1 (void);
// 0x0000001C System.Void PoolManager::NullChecker()
extern void PoolManager_NullChecker_mDC52D6FE9974001820CE79EF3B8FA695EB3FE244 (void);
// 0x0000001D System.Void PoolManager::CreatePrefabs()
extern void PoolManager_CreatePrefabs_m4383503766154182F52FBE49E29B827058449673 (void);
// 0x0000001E UnityEngine.GameObject PoolManager::spawnCow()
extern void PoolManager_spawnCow_m39B59B1BF6976D4EC142B8E03BAAFC944BFF68B0 (void);
// 0x0000001F UnityEngine.GameObject PoolManager::spawnTruck()
extern void PoolManager_spawnTruck_m450B12FE78F42B44AB6B83008D5FE5FB970BA2B6 (void);
// 0x00000020 UnityEngine.GameObject PoolManager::spawnBus()
extern void PoolManager_spawnBus_m05DAC717B34317163E7954503C7AD1DB84E49F7C (void);
// 0x00000021 UnityEngine.GameObject PoolManager::spawnManhole()
extern void PoolManager_spawnManhole_mC2C3D0EE5EC51A355145C3ADD91697ACF3CD5DCC (void);
// 0x00000022 System.Void PoolManager::.ctor()
extern void PoolManager__ctor_mB63FFA1BD11E3C07306CA9E388496F85BD09A7FF (void);
// 0x00000023 System.Void PoolManager::.cctor()
extern void PoolManager__cctor_mB07CD4906E8EED5D07CBE69C9532E0EB7F33205C (void);
// 0x00000024 UnityEngine.Vector2 Swipe::get_SwipeDelta()
extern void Swipe_get_SwipeDelta_mBED3BCB874D30261CAC585DDA86EEC3123683122 (void);
// 0x00000025 System.Boolean Swipe::get_SwipeLeft()
extern void Swipe_get_SwipeLeft_m376E4EF580BA7F21047F34F4555E4F5D70CAB00E (void);
// 0x00000026 System.Boolean Swipe::get_SwipeRight()
extern void Swipe_get_SwipeRight_m0F78E468AAD5E39AE3891FD8B0A3249C2A9475FF (void);
// 0x00000027 System.Boolean Swipe::get_SwipeUp()
extern void Swipe_get_SwipeUp_m86FC469F91F9564148A6BFD59AE68CB83A25B42C (void);
// 0x00000028 System.Void Swipe::Update()
extern void Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD (void);
// 0x00000029 System.Void Swipe::ResetTouch()
extern void Swipe_ResetTouch_m5B17F96D57FAB05EA7ECA8FF0F9B4AE51931FB6A (void);
// 0x0000002A System.Void Swipe::.ctor()
extern void Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B (void);
// 0x0000002B System.Void Background::Start()
extern void Background_Start_m0809057BDAE7740DFDFE70B2DFB9ADF9DAA26D76 (void);
// 0x0000002C System.Void Background::LateUpdate()
extern void Background_LateUpdate_m4721B57E75785B94FBF9CB5477761B1BEF8022D5 (void);
// 0x0000002D System.Void Background::.ctor()
extern void Background__ctor_mEC9A50E3A2C20E886BD7FA5D93F73FF0CC9E7B38 (void);
// 0x0000002E System.Void CameraFollow::Start()
extern void CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D (void);
// 0x0000002F System.Void CameraFollow::Update()
extern void CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08 (void);
// 0x00000030 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x00000031 System.Void Obstacle::Start()
extern void Obstacle_Start_mB31D51F665C9770319F028D5F370D5F1B39EF370 (void);
// 0x00000032 System.Void Obstacle::Update()
extern void Obstacle_Update_m926110BAC1589369A867CEEDA4BB1A8BF553415B (void);
// 0x00000033 System.Collections.IEnumerator Obstacle::waitSpawner()
extern void Obstacle_waitSpawner_m80ACFF957BF81424A4478DD03B78E1E4547375DE (void);
// 0x00000034 System.Void Obstacle::.ctor()
extern void Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96 (void);
// 0x00000035 System.Void Obstacle/<waitSpawner>d__13::.ctor(System.Int32)
extern void U3CwaitSpawnerU3Ed__13__ctor_mDC5B31D714C04512BA49462112CCD9AE228A3393 (void);
// 0x00000036 System.Void Obstacle/<waitSpawner>d__13::System.IDisposable.Dispose()
extern void U3CwaitSpawnerU3Ed__13_System_IDisposable_Dispose_m198BAD30F854F4975795008E9567E6FFE5C9B3B0 (void);
// 0x00000037 System.Boolean Obstacle/<waitSpawner>d__13::MoveNext()
extern void U3CwaitSpawnerU3Ed__13_MoveNext_m758BA6FCAF5BB563CEFD73472E74C9827D024E36 (void);
// 0x00000038 System.Object Obstacle/<waitSpawner>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CwaitSpawnerU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A1ACCE69E8E9EA9E4ED8B1565620B9747F27D9D (void);
// 0x00000039 System.Void Obstacle/<waitSpawner>d__13::System.Collections.IEnumerator.Reset()
extern void U3CwaitSpawnerU3Ed__13_System_Collections_IEnumerator_Reset_m6A263416AE8F68E7F8D501DCC130E03DA51D17D4 (void);
// 0x0000003A System.Object Obstacle/<waitSpawner>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CwaitSpawnerU3Ed__13_System_Collections_IEnumerator_get_Current_mF4DB7D0F70F7CA1BCE818164E6CD4BEF2188D71E (void);
// 0x0000003B System.Void Player::Awake()
extern void Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24 (void);
// 0x0000003C System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x0000003D System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x0000003E System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088 (void);
// 0x0000003F System.Void Player::PlayerDirectionChanger()
extern void Player_PlayerDirectionChanger_mEBEF0B096910916C19E6B53C4FAB987DE048906B (void);
// 0x00000040 System.Void Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player_OnTriggerEnter2D_mAF357F7244427CB9EADB81B5A6C4F0AF481641D0 (void);
// 0x00000041 System.Void Player::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90 (void);
// 0x00000042 System.Void Player::OnCollisionExit2D(UnityEngine.Collision2D)
extern void Player_OnCollisionExit2D_mF2E5300C9E75724B39C03438EAA1024713F63A99 (void);
// 0x00000043 System.Void Player::Jump()
extern void Player_Jump_m741715891EE6DEC1EC5F6A7233B6395ABD0E272C (void);
// 0x00000044 System.Void Player::increaseJump()
extern void Player_increaseJump_mE953D4F064628D5E28F5DEA805BFBFF21B0C4EE6 (void);
// 0x00000045 System.Void Player::decreaserJump()
extern void Player_decreaserJump_m5489CD6A586209406A92866ED890C3A4E1E3530F (void);
// 0x00000046 System.Void Player::resetJump()
extern void Player_resetJump_m7F0DFC2E1F616391C323D9E0983C8371F5D83E14 (void);
// 0x00000047 System.Void Player::Move(UnityEngine.Vector3)
extern void Player_Move_m87A00C717192266743C5E901EA8A7394C8F8521C (void);
// 0x00000048 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000049 System.Void Player::.cctor()
extern void Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F (void);
// 0x0000004A System.Void PlayerHealth::Start()
extern void PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589 (void);
// 0x0000004B System.Void PlayerHealth::OnTriggerEnter(UnityEngine.Collider)
extern void PlayerHealth_OnTriggerEnter_m730F370538F0D6B9B129361921CABEAE89F2CD47 (void);
// 0x0000004C System.Void PlayerHealth::.ctor()
extern void PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1 (void);
// 0x0000004D System.Void RayCastNoOverlap::Start()
extern void RayCastNoOverlap_Start_m568466F38C7457F56953465BD30FFA944DC03DF5 (void);
// 0x0000004E System.Void RayCastNoOverlap::PositionRaycast()
extern void RayCastNoOverlap_PositionRaycast_m6D36E7632B5924A14FB5844228FE1D62646D92DF (void);
// 0x0000004F System.Void RayCastNoOverlap::Pick(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void RayCastNoOverlap_Pick_m3F2046485CBF72CAA579F92CFD6443CEC3CA5D9E (void);
// 0x00000050 System.Void RayCastNoOverlap::.ctor()
extern void RayCastNoOverlap__ctor_m92C41041E523B0A500788C52625E56CE789ABB42 (void);
// 0x00000051 System.Void SwipeDetection::Update()
extern void SwipeDetection_Update_mB4D4FEE6A6CBF78B7CC5FBDE239124262E1E9EC5 (void);
// 0x00000052 System.Void SwipeDetection::Detection()
extern void SwipeDetection_Detection_m62E86D6264A0FC12BA9DCC25B7C3213000DEB46F (void);
// 0x00000053 System.Void SwipeDetection::.ctor()
extern void SwipeDetection__ctor_m2C4513A493B17DBB06F395DCEA18547D30EC2D58 (void);
static Il2CppMethodPointer s_methodPointers[83] = 
{
	BackGround_parallax_Start_mD4AC5930AF76396735290F1AD743EA7903A82110,
	BackGround_parallax_Update_m84021604B415F4C5F362A4D9D4ADD9798175C803,
	BackGround_parallax__ctor_m8F34171063BC9B4BD7CB79C1094CFE43307E9A6F,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_Restart_m1E9741B5443E1FF65B0BE1A20A8F584C90394654,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	GameManager__cctor_m0872BB5B2E20B9183F004EB81D5129608114D285,
	ObsatcleSpawner_OnEnable_m50FCC755781FEE071FAD9473951A75D227223D5A,
	ObsatcleSpawner_Start_m27EA92EBEEB031CE9408B7E398A26CA6E40AE6CE,
	ObsatcleSpawner_Spawner_mBEFCD361A47CA2E056CB3B5ADF2963B79F82F05C,
	ObsatcleSpawner__ctor_m4928465F57D2F3EB03D81282E86BA7958DB4FE8B,
	U3CSpawnerU3Ed__7__ctor_mA6797C46ACF74131A3985AFD7321DC257B8FC5F8,
	U3CSpawnerU3Ed__7_System_IDisposable_Dispose_mD528048A4D6B2750F82476C03E26D5D5500C008F,
	U3CSpawnerU3Ed__7_MoveNext_mDFC28C95F5DEE4A504E32C44D1E0231799E82C56,
	U3CSpawnerU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF51FAF03F53A2B24C7FEBB503CACDFDE3C45013C,
	U3CSpawnerU3Ed__7_System_Collections_IEnumerator_Reset_m43B5CB3EC7C781E60243B53A52E33129008BFC84,
	U3CSpawnerU3Ed__7_System_Collections_IEnumerator_get_Current_m31FEB79F32570644AC62FFD36984615EA9394524,
	ObstacleProperties_OnEnable_mF6B61D2B058D55BA245BC0F4282A18E7194DDE07,
	ObstacleProperties_Update_m668DC200D060A2CFBD3E091A14D75323EC45315E,
	ObstacleProperties__ctor_mBEEDBE4F11963E6A274E7A13AE1B0D403216030A,
	OnInvisible_LateUpdate_mB0614738B7C25BFADC80EFECCF7DA309D6ABF634,
	OnInvisible_OnBecameInvisible_mAD9D7123BD6721FA3917E0933D95FDDFF916F8BF,
	OnInvisible__ctor_m1D1432C7922295E59AD498CBAA2DB00EABB8E58F,
	PoolManager_Awake_m786B4A25FE5ECC36089F119C13947812B49CC13C,
	PoolManager_Start_m99A69714EE1C8A780EBABB4006D309BF67A56DF1,
	PoolManager_NullChecker_mDC52D6FE9974001820CE79EF3B8FA695EB3FE244,
	PoolManager_CreatePrefabs_m4383503766154182F52FBE49E29B827058449673,
	PoolManager_spawnCow_m39B59B1BF6976D4EC142B8E03BAAFC944BFF68B0,
	PoolManager_spawnTruck_m450B12FE78F42B44AB6B83008D5FE5FB970BA2B6,
	PoolManager_spawnBus_m05DAC717B34317163E7954503C7AD1DB84E49F7C,
	PoolManager_spawnManhole_mC2C3D0EE5EC51A355145C3ADD91697ACF3CD5DCC,
	PoolManager__ctor_mB63FFA1BD11E3C07306CA9E388496F85BD09A7FF,
	PoolManager__cctor_mB07CD4906E8EED5D07CBE69C9532E0EB7F33205C,
	Swipe_get_SwipeDelta_mBED3BCB874D30261CAC585DDA86EEC3123683122,
	Swipe_get_SwipeLeft_m376E4EF580BA7F21047F34F4555E4F5D70CAB00E,
	Swipe_get_SwipeRight_m0F78E468AAD5E39AE3891FD8B0A3249C2A9475FF,
	Swipe_get_SwipeUp_m86FC469F91F9564148A6BFD59AE68CB83A25B42C,
	Swipe_Update_m5B8885067E3CB784950DB863B1BDDDA26652D0BD,
	Swipe_ResetTouch_m5B17F96D57FAB05EA7ECA8FF0F9B4AE51931FB6A,
	Swipe__ctor_m97873674A6FB4BD351B24309C91C278A784F013B,
	Background_Start_m0809057BDAE7740DFDFE70B2DFB9ADF9DAA26D76,
	Background_LateUpdate_m4721B57E75785B94FBF9CB5477761B1BEF8022D5,
	Background__ctor_mEC9A50E3A2C20E886BD7FA5D93F73FF0CC9E7B38,
	CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D,
	CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	Obstacle_Start_mB31D51F665C9770319F028D5F370D5F1B39EF370,
	Obstacle_Update_m926110BAC1589369A867CEEDA4BB1A8BF553415B,
	Obstacle_waitSpawner_m80ACFF957BF81424A4478DD03B78E1E4547375DE,
	Obstacle__ctor_m8DAB1C8ECB2328D09E320C484CC0C76D7C474C96,
	U3CwaitSpawnerU3Ed__13__ctor_mDC5B31D714C04512BA49462112CCD9AE228A3393,
	U3CwaitSpawnerU3Ed__13_System_IDisposable_Dispose_m198BAD30F854F4975795008E9567E6FFE5C9B3B0,
	U3CwaitSpawnerU3Ed__13_MoveNext_m758BA6FCAF5BB563CEFD73472E74C9827D024E36,
	U3CwaitSpawnerU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9A1ACCE69E8E9EA9E4ED8B1565620B9747F27D9D,
	U3CwaitSpawnerU3Ed__13_System_Collections_IEnumerator_Reset_m6A263416AE8F68E7F8D501DCC130E03DA51D17D4,
	U3CwaitSpawnerU3Ed__13_System_Collections_IEnumerator_get_Current_mF4DB7D0F70F7CA1BCE818164E6CD4BEF2188D71E,
	Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088,
	Player_PlayerDirectionChanger_mEBEF0B096910916C19E6B53C4FAB987DE048906B,
	Player_OnTriggerEnter2D_mAF357F7244427CB9EADB81B5A6C4F0AF481641D0,
	Player_OnCollisionEnter2D_m0448BB6F02AAB4967D3AB976DA22A24ACD8E7F90,
	Player_OnCollisionExit2D_mF2E5300C9E75724B39C03438EAA1024713F63A99,
	Player_Jump_m741715891EE6DEC1EC5F6A7233B6395ABD0E272C,
	Player_increaseJump_mE953D4F064628D5E28F5DEA805BFBFF21B0C4EE6,
	Player_decreaserJump_m5489CD6A586209406A92866ED890C3A4E1E3530F,
	Player_resetJump_m7F0DFC2E1F616391C323D9E0983C8371F5D83E14,
	Player_Move_m87A00C717192266743C5E901EA8A7394C8F8521C,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	Player__cctor_m38DB9DEEDAE88802EC9823ECFF4A17859AABF96F,
	PlayerHealth_Start_m78FD812EF2B87E9EC7A405A1BBB6ECB27BFF3589,
	PlayerHealth_OnTriggerEnter_m730F370538F0D6B9B129361921CABEAE89F2CD47,
	PlayerHealth__ctor_mE9AF3CA69205909E44287664BEAE503EC43875F1,
	RayCastNoOverlap_Start_m568466F38C7457F56953465BD30FFA944DC03DF5,
	RayCastNoOverlap_PositionRaycast_m6D36E7632B5924A14FB5844228FE1D62646D92DF,
	RayCastNoOverlap_Pick_m3F2046485CBF72CAA579F92CFD6443CEC3CA5D9E,
	RayCastNoOverlap__ctor_m92C41041E523B0A500788C52625E56CE789ABB42,
	SwipeDetection_Update_mB4D4FEE6A6CBF78B7CC5FBDE239124262E1E9EC5,
	SwipeDetection_Detection_m62E86D6264A0FC12BA9DCC25B7C3213000DEB46F,
	SwipeDetection__ctor_m2C4513A493B17DBB06F395DCEA18547D30EC2D58,
};
static const int32_t s_InvokerIndices[83] = 
{
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	2776,
	1745,
	1745,
	1689,
	1745,
	1407,
	1745,
	1712,
	1689,
	1745,
	1689,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1689,
	1689,
	1689,
	1689,
	1745,
	2776,
	1738,
	1712,
	1712,
	1712,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1745,
	1689,
	1745,
	1407,
	1745,
	1712,
	1689,
	1745,
	1689,
	1745,
	1745,
	1745,
	1745,
	1745,
	1417,
	1417,
	1417,
	1745,
	1745,
	1745,
	1745,
	1465,
	1745,
	2776,
	1745,
	1417,
	1745,
	1745,
	1745,
	822,
	1745,
	1745,
	1745,
	1745,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	83,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
