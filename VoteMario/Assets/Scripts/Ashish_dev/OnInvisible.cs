using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnInvisible : MonoBehaviour
{

    private void LateUpdate()
    {
        OnBecameInvisible();
    }

    private void OnBecameInvisible()
    {
        transform.position = new Vector3(transform.GetSiblingIndex() * 19f * Player.Direction, transform.position.y);
    }
}
