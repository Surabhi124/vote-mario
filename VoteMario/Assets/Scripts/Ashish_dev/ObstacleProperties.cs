using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleProperties : MonoBehaviour
{
    public float LASTLIFE;
    Player playerref;

    private void OnEnable()
    {
        LASTLIFE = LASTLIFE <= 0 ? 2f : LASTLIFE;
        if(playerref==null)
            playerref = FindObjectOfType<Player>();
    }

    private void Update()
    {
        if (Mathf.Abs(transform.position.x - playerref.transform.position.x) >= 20f)
            gameObject.SetActive(false);
    }
}
