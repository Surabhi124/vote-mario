using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    bool sl, sr, su,touched,dragg;
    [SerializeField] Player pl = null;
    Vector2 startTouch, swipeDelta;

    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeLeft { get { return sl; } }
    public bool SwipeRight { get { return sr; } }
    public bool SwipeUp { get { return su; } }

    private void Update()
    {
        touched = sr = sl = su = false;

        #region Mouse Input
        if (Input.touches.Length > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                touched = dragg = true;
                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                touched = dragg = false;
                ResetTouch();
            }
        }

        #endregion


        swipeDelta = Vector2.zero;
        if (dragg)
        {
            if (Input.touches.Length > 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;
            }
        }

        //direction check karo yaha
        if (swipeDelta.magnitude > 125)
        {
            float x = swipeDelta.x, y = swipeDelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0)
                {
                    sl = true;
                    sr = false;
                }
                if (x > 0)
                {
                    sr = true;
                    sl = false;
                }
            }
            else
            {
                if (y > 0)
                    su = true;
            }
            ResetTouch();
        }
        if (sl)
        {
            Player.Direction = -1;
        }
        if (sr)
        {
            Player.Direction = 1;
        }
        if(su)
        {
            pl.Jump();
            pl.isGrounded = false;
        }
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pl.Jump();
            pl.isGrounded = false;
        }
#endif
    }


    void ResetTouch()
    {
        startTouch = swipeDelta = Vector2.zero;
        dragg = false;
        }
}
