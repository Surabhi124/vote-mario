using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsatcleSpawner : MonoBehaviour
{
    [Header("Script referneces")]
    public PoolManager pm;
    public Player player_ref;

    [Header("Timers for spawning")]
    [Tooltip("The obstacles will spawn within these limits")]
    [SerializeField] float UpperLimit;
    [SerializeField] float LowerLimit;

    [Header("Proximity")]
    [Tooltip("This will define how far the obstacles will spawn from players' current position and direction")]
    [SerializeField] float Offset;
    private void OnEnable()
    {
        Debug.LogError("ObsSpa is running now");
    }

    private void Start()
    {
        StartCoroutine(Spawner());
        Offset = Offset == 0 ? 5f : Offset;
    }

    IEnumerator Spawner()
    {
        var temp = Mathf.Round(Random.Range(LowerLimit, UpperLimit) * 100f) / 100f;
        //yield return new WaitForSeconds(.2f);
        GameObject theObject = null;
        //magik
        float rand = Random.Range(1, 5);             // change 5 --> no of obstacles
        switch (rand)
        {
            case 1:
                theObject = PoolManager.instance_.spawnCow();
                break;
            case 2:
                theObject = PoolManager.instance_.spawnBus();
                break;
            case 3:
                theObject = PoolManager.instance_.spawnManhole();
                break;
            case 4:
                theObject = PoolManager.instance_.spawnTruck();
                break;
            default:
                theObject = null;
                break;
        }
        Debug.Log(theObject.name + ", randomizer value --> " +rand);
        yield return new WaitForSeconds(temp);
        if (theObject != null)
        {
            theObject.SetActive(true);
            theObject.transform.position = new Vector2((Player.Direction * Offset) + player_ref.transform.position.x, theObject.transform.position.y);
        }
        StartCoroutine(Spawner());
    }
}
