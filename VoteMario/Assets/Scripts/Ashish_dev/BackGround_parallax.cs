//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//public class BackGround_parallax : MonoBehaviour
//{
//    //[SerializeField] GameObject houseSprite = null, floorSprite = null, skySprite = null,backgroundBlackSprite = null,treesSprite = null;
//    //public GameObject playerSPrite = null;

//    //[SerializeField] float houseSpeed = 0f, skySpeed = 0f, GroundSpeed = 0f, backgroundSpeed = 0f, treesSpeed = 0f;



//    //Vector3 cameraLastPosition;

//    private void Awake()
//    {
//        //if (houseSprite == null || floorSprite == null || skySprite == null|| treesSprite ==null)
//        //    print("something is null");
//        //if (backgroundBlackSprite == null)
//        //    print("BG list is null");
//        //if (playerSPrite == null)
//        //    print("PLAYER is " + playerSPrite);
//        //cameraLastPosition = Camera.main.transform.position;
//    }

//    private void Update()
//    {

//    }

//    private void LateUpdate()
//    {
//        //MoveAllTheElements(Player.Direction);
//        //RepeatingBackground(Player.Direction);
//    }
//    #region Parallax
//    void MoveAllTheElements(int Dir)
//    {
//       // backgroundBlackSprite.transform.position += new Vector3(-Dir * backgroundSpeed*Time.deltaTime, 0f);
//       // houseSprite.transform.position += new Vector3(-Dir * houseSpeed * Time.deltaTime, 0f);
//       //// floorSprite.transform.position += new Vector3(-Dir * GroundSpeed * Time.deltaTime, 0f);
//       // skySprite.transform.position += new Vector3(-Dir * skySpeed * Time.deltaTime, 0f);
//       // treesSprite.transform.position += new Vector3(-Dir * treesSpeed * Time.deltaTime, 0f);
//    }

//    void RepeatingBackground(int Dir)
//    {
//        //float BGLength = houseSprite.transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x * 3f;
//        //GameObject temp = null;
//        //if (playerSPrite.transform.position.x * Player.Direction >= BGLength * .5f && temp == null)
//        //{
//        //    temp = Instantiate(houseSprite,new Vector3(BGLength * Player.Direction, houseSprite.transform.position.y), Quaternion.identity);
//        //    temp.transform.SetParent(houseSprite.transform);
//        //}
//        //float FLLength = floorSprite.transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x * 3f;
//    }
//    #endregion
//}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround_parallax : MonoBehaviour
{
    float length, startposition, relativePos;
    public GameObject Cam;
    [SerializeField] float parallaxPower;

    private void Start()
    {
        startposition = transform.position.x;
       // Debug.LogFormat("{0} with {1}", gameObject.name.ToString(), startposition.ToString());
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    #region Parallax
    private void Update()
    {
        relativePos = FindObjectOfType<Player>().gameObject.transform.position.x * (1 - parallaxPower);
        float dist = FindObjectOfType<Player>().gameObject.transform.position.x * parallaxPower;
        transform.position = new Vector3(startposition + dist, transform.position.y);


        if (relativePos > startposition + length)
            startposition += length;
        else if (relativePos < startposition - length)
            startposition -= length;
    }
    #endregion
}
