using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public static PoolManager instance_ = null;

    [SerializeField] GameObject cow = null, bus = null, truck = null,manhole = null;        // add goons and others later
    [SerializeField] Transform SpawnHolder = null;
    [SerializeField] int cow_copies = 1, bus_copies = 1, truck_copies = 1, manhole_copies = 1;

    //private GameObject temp = null;
    List<GameObject> cowList, busList, truckList, manholeList;

    private void Awake()
    {
        if (instance_ != this || instance_ == null)
        {
            instance_ = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        CreatePrefabs();
    }


    void NullChecker()
    {
        if (cow == null)
            print("cow is null");
        if (truck == null)
            print("truck is null");
        if (bus == null)
            print("bus is null");
        if (manhole == null)
            print("manhole is null");
    }

    void CreatePrefabs()
    {
        GameObject t1,t2,t3, t4;
        int i;
        cowList = new List<GameObject>(cow_copies);
        for (i = 0; i < cow_copies; i++)
        {
            t1 = Instantiate(cow, SpawnHolder);
            t1.SetActive(false);
            cowList.Add(t1);
        }
        truckList = new List<GameObject>(truck_copies);
        for (i = 0; i < truck_copies; i++)
        {
            t2 = Instantiate(truck, SpawnHolder);
            t2.SetActive(false);
            truckList.Add(t2);
        }
        busList = new List<GameObject>(bus_copies);
        for (i = 0; i < bus_copies; i++)
        {
            t3 = Instantiate(bus, SpawnHolder);
            t3.SetActive(false);
            busList.Add(t3);
        }
        manholeList = new List<GameObject>(manhole_copies) ;
        for (i = 0; i < manhole_copies; i++)
        {
            t4 = Instantiate(manhole, SpawnHolder);
            t4.SetActive(false);
            manholeList.Add(t4);
        }
        t1 = t2 = t3 = t4 = null;
    }

    public GameObject spawnCow()
    {
        GameObject temp= null;
        int i;
        for (i = 0; i < cowList.Count; i++)
        {
            if (!cowList[i].activeSelf)
            {
                temp = cowList[i];
                break;
            }
        }
        if (temp == null && cowList.Count <= cow_copies*2)
        {
            temp = Instantiate(cow, SpawnHolder);
            temp.SetActive(false);
            cowList.Add(temp);
        }
        //temp = cowList[1];
        temp.SetActive(true);
        //Debug.Log("1");
        return temp;
    }

    public GameObject spawnTruck()
    {
        GameObject temp= null;
        int i;
        for (i = 0; i < truckList.Count; i++)
        {
            if (!truckList[i].activeSelf)
            {
                temp = truckList[i];
                break;
            }
        }
        if (temp == null && truckList.Count <= truck_copies*2)
        {
            temp = Instantiate(truck, SpawnHolder);
            temp.SetActive(false);
            truckList.Add(temp);
        }
        temp.SetActive(true);
        //Debug.Log("4");
        return temp;
    }
    public GameObject spawnBus()
    {
        GameObject temp= null;
        int i;
        for (i = 0; i < busList.Count; i++)
        {
            if (!busList[i].activeSelf)
            {
                temp = busList[i];
                break;
            }
        }
        if (temp == null && busList.Count <= bus_copies*2)
        {
            temp = Instantiate(bus, SpawnHolder);
            temp.SetActive(false);
            busList.Add(temp);
        }
        temp.SetActive(true);
        //Debug.Log("2");
        return temp;
    }

    public GameObject spawnManhole()
    {
        GameObject temp = null;
        int i;
        for (i = 0; i < manholeList.Count; i++)
        {
            if (!manholeList[i].activeSelf)
            {
                temp = manholeList[i];
                break;
            }
        }
        if (temp == null && manholeList.Count <= manhole_copies*2)
        {
            temp = Instantiate(manhole, SpawnHolder);
            temp.SetActive(false);
            manholeList.Add(temp);
        }
        temp.SetActive(true);
        //Debug.Log("3");
        return temp;
    }
}
