using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    SpriteRenderer sr;
    [SerializeField] float movementSpeed = 6f, jumpSpeed = 0f,xpowerOnjump = 1;
    float initMovespeed, initJump, initXpower;
    public static int Direction = 1;

    private Vector3 targetPos;
    public bool isGrounded = true;
    /*
        public float maxSwipetime;
        public float minSwipeDistance;
        private float swipeStartTime;
        private float swipeEndTime;
        private float swipeTime;
        private Vector2 startSwipePosition;
        private Vector2 endSwipePosition;
        private float swipeLength;*/
    // Start is called before the first frame update

    private void Awake()
    {
        initJump = jumpSpeed;
        initMovespeed = movementSpeed;
        initXpower = xpowerOnjump;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        targetPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //float horizontalInput = Input.GetAxis("Horizontal");
        //float verticalInput = Input.GetAxis("Vertical");

        //rb.velocity = new Vector3(horizontalInput * movementSpeed, rb.velocity.y, verticalInput * movementSpeed);
        PlayerDirectionChanger();
        print(Direction + " <-- Player.Direction");
        // TouchMove();

       // transform.position = Vector3.MoveTowards(transform.position, targetPos, moveSpeed * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(movementSpeed * Direction, rb.velocity.y);
    }
    void PlayerDirectionChanger()
    {
         if (Input.GetKeyDown(KeyCode.A))
        {
            Direction = -1;
        }

        else if (Input.GetKeyDown(KeyCode.D))
        {
            Direction = 1;
        }
        sr.flipX = Direction == -1 ? true : false;
        
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {

        string tagName = col.gameObject.tag;

        if (tagName == "Manhole")
        {
            GetComponent<Rigidbody2D>().gravityScale = 0f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Floor") || collision.transform.tag == "Obstacle")
        {
            isGrounded = true;
            movementSpeed = initMovespeed;         
        }
        //if (collision.transform.tag == "Obstacle")
        //{
        //    //moveSpeed = 0f;
        //    //rb.velocity = Vector2.zero;
        //    //GameManager.instance_.GameOver = true;
        //    isGrounded = true;
        //}
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Floor"))
        {
            isGrounded = false;
        }
    }

    public void Jump()
    {
        if (/*Input.GetKeyDown(KeyCode.Space) && */isGrounded)
        {
            movementSpeed = xpowerOnjump ;
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpSpeed), ForceMode2D.Impulse);
        }
    }

    public void increaseJump()
    {
        jumpSpeed += 2f;
        xpowerOnjump++;
    }
    public void decreaserJump()
    {
        jumpSpeed -= 2f;
        xpowerOnjump--;
    }
    public void resetJump()
    {
        jumpSpeed = initJump;
        xpowerOnjump = initXpower;
    }
        /*
    void TouchMove()
    {
        if(Input.GetMouseButton(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(mousePos.x > 1)
            {
                transform.Translate(moveSpeed, 0, 0);
                Debug.Log("touch detected");
            }
            if (mousePos.x < -1)
            {
                transform.Translate(-moveSpeed, 0, 0);
                Debug.Log("left touch detected");
            }
        }
    }*/
   /* void SwipeTest()
    {
        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                swipeStartTime = Time.time;
                startSwipePosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                swipeEndTime = Time.time;
                endSwipePosition = touch.position;
                swipeTime = swipeEndTime - swipeStartTime;
                swipeLength = (endSwipePosition - startSwipePosition).magnitude;
                if(swipeTime<maxSwipetime && swipeLength > minSwipeDistance)
                {
                    swipeControl();
                }
            }
        }
    }

    void swipeControl()
    {
        Vector2 Distance = endSwipePosition - startSwipePosition;
        float xDistance = Mathf.Abs(Distance.x);
        float yDistance = Mathf.Abs(Distance.y);
        if(xDistance > yDistance)
        {
            if(Distance.x > 0 && (Input.GetKeyDown(KeyCode.A)))
            {
                PlayerDirectionChanger();
            }
            else if (Distance.x < 0 && (Input.GetKeyDown(KeyCode.D)))
            {
                PlayerDirectionChanger();
            }

        }
    }*/
    public void Move(Vector3 moveDir)
    {
        targetPos += moveDir;
    }
}




// add jopystick.
// player speed >=0