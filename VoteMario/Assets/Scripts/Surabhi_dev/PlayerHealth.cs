using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int health = 100;
    public Text healthText;


    // Use this for initialization
    void Start()
    {
        healthText = GetComponent<Text>();
        healthText.text = "Health: " + health;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Obstacle")
        {
            health -=20;
            healthText = GetComponent<Text>();
            healthText.text = "Health: " + health;
        }
    }
} 