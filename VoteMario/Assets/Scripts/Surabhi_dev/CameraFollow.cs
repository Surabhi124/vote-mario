using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    /*
    public Transform target;
    public Vector3 offset;
    [Range(1, 10)]
    public float smoothFactor;
  
    void Start() { Follow(); }

    private void FixedUpdate()
    {
        Follow();
    }

    void Follow()
    {
        Vector3 targetPosition = target.position + offset;
     
        Vector3 smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.fixedDeltaTime);
        transform.position = smoothPosition;

    }*/

    /*
    private GameObject player;
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    void Start()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
    }
    void LateUpdate()
    {
        float x = Mathf.Clamp(player.transform.position.x, xMin, xMax);
        float y = Mathf.Clamp(player.transform.position.y, yMin, yMax);
        gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
    }*/


    /*
    
    public GameObject followObject;
    public Vector2 followOffset;
    public float speed = 3f;
    private Vector2 threshold;
    private Rigidbody2D rb;
  //  private GameObject player;
   // public float xMin;
   // public float xMax;
    //public float yMin;
   // public float yMax;
    void Start()
    {

        //player = GameObject.FindGameObjectWithTag("Player");

        threshold = calculateThreshold();
        rb = followObject.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        Vector2 follow = followObject.transform.position;
        float xDifference = Vector2.Distance(Vector2.right * transform.position.x, Vector2.right * follow.x);
        float yDifference = Vector2.Distance(Vector2.up * transform.position.y, Vector2.up * follow.y);

        Vector3 newPosition = transform.position;
        if (Mathf.Abs(xDifference) >= threshold.x)
        {
            newPosition.x = follow.x;
        }
        if (Mathf.Abs(yDifference) >= threshold.y)
        {
            newPosition.y = follow.y;
        }
        float moveSpeed = rb.velocity.magnitude > speed ? rb.velocity.magnitude : speed;
        transform.position = Vector3.MoveTowards(transform.position, newPosition, moveSpeed * Time.deltaTime);
    }

    private Vector3 calculateThreshold()
    {
        Rect aspect = Camera.main.pixelRect;
        Vector2 t = new Vector2(Camera.main.orthographicSize * aspect.width / aspect.height, Camera.main.orthographicSize);
        t.x -= followOffset.x;
        t.y -= followOffset.y;
        return t;
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Vector2 border = calculateThreshold();
        Gizmos.DrawWireCube(transform.position, new Vector3(border.y * 2, border.x * 2, 1));
    }
    */



    /*
    public Transform player;
    public Vector3 cameraOffset;
    public float cameraSpeed = 0.1f;

    void Start()
    {
        transform.position = player.position + cameraOffset;
    }

    void FixedUpdate()
    {
        Vector3 finalPosition = player.position + cameraOffset;
        Vector3 lerpPosition = Vector3.Lerp(transform.position, finalPosition, cameraSpeed);
        transform.position = lerpPosition;
    }*/
    /*
        //private GameObject player;
        public float xMin;
        public float xMax;
        public float yMin;
        public float yMax;
        public Transform player;
        public Vector3 cameraOffset;
        public float cameraSpeed = 0.1f;
        void Start()
        {
           // player = GameObject.FindGameObjectWithTag("Player");
            transform.position = player.position + cameraOffset;
        }


        void FixedUpdate()
        {
            float x = Mathf.Clamp(player.transform.position.x, xMin, xMax);
            //float y = Mathf.Clamp(player.transform.position.y, yMin, yMax);
           // gameObject.transform.position = new Vector3(x, y, gameObject.transform.position.z);
            Vector3 finalPosition = player.position + cameraOffset;
            Vector3 lerpPosition = Vector3.Lerp(transform.position, finalPosition, cameraSpeed);
            transform.position = lerpPosition;
        }*/

    /*
    [SerializeField]
    private float speed;
    private float currentPosX;
    private Vector3 velocity = Vector3.zero;

    [SerializeField] private Transform player;
    [SerializeField] private float ahead_dist;
    [SerializeField] private float camSpeed;
    private float lookahead;

    private void Update()
    {
        transform.position = new Vector3(player.position.x + lookahead, transform.position.y, transform.position.z);
        lookahead = Mathf.Lerp(lookahead, (ahead_dist * player.localScale.x), Time.deltaTime * camSpeed);
    }

    public void Movenew(Transform _new)
    {
        currentPosX = _new.position.x;
    }
    */


    /*
    public Player player;
    public float timeOffset = 2.5f;
    public bool canMoveY = false;

    public Vector3 posOffset;

    private void FixedUpdate()
    {
        Vector3 startPosition = transform.position;
        Vector3 endPosition = player.transform.position;

        if (Input.GetKeyDown(KeyCode.A))
        {
            endPosition.x += posOffset.x;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            endPosition.x -= posOffset.x;
        }

        if (!canMoveY)
        {
            endPosition.y = 0;
        }
        endPosition.z = -10;

        transform.position = Vector3.Lerp(startPosition, endPosition, Time.fixedDeltaTime * timeOffset);
    }*/
    public GameObject player;
    public float offset;
    private Vector3 playerPosition;
    public float offsetSmoothing;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        playerPosition = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
       
        if (player.transform.localScale.x > 0f)
        {
            playerPosition = new Vector3(playerPosition.x - offset*Player.Direction, playerPosition.y, playerPosition.z);
        }
        else 
        {
            return;
            //playerPosition = new Vector3(playerPosition.x + offset, playerPosition.y, playerPosition.z);
        }
        transform.position = Vector3.Lerp(transform.position, playerPosition, offsetSmoothing * Time.deltaTime);
    }
}